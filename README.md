# GoodFood - C#

## Description
GoodFood est une solution de livraison de plats cuisinés.

Cette solution comporte un site web ainsi qu'une application mobile, et est composée de plusieurs micro services effectuant des actions spécifiques.

Dans ce repository, le microservice .NET gère les utilisateurs de la solution GoodFood.

Les fonctionnalités concernant les utilisateurs seront explicitées dans la prochaine partie.

### Fonctionnalités
Le microservice .NET inclus les fonctionnalités suivantes :

* **Un système d'inscription** : les utilisateurs peuvent se créer un compte via l'API .NET.
* **Un système d'authentification** : celui-ci est basé sur l'adresse e-mail de l'utilisateur ainsi que son mot de passe. Lorsque les identifiants sont corrects, un JWT est renvoyé, ainsi qu'un jeton de rafraichissement du JWT.
* **Rafraichissement du JWT** : le JWT ayant une durée de vie limitée, ce dernier peut être regénéré grâce a un jeton délivré durant l'authentification de l'utilisateur.
* **Modification des données utilisateur** : l'utilisateur a la possibilité de modifier ses données personnelles (nom, prénom, mot de passe et photo de profil).
* **Suppression du compte utilisateur** : l'utilisateur a la possibilité de supprimer son compte utilisateur.

### Routes
**Important** : L'URL de base est localhost:5001 si le protocole est HTTPS ou localhost:5000 pour le protocole HTTP.

* **Inscription** : /api/users/registration
* **Authentification** : /api/users/login
* **Rafraichissement JWT** : /api/users/refresh_token
* **Modification de la photo de profil** : /api/users/avatar/{id}
* **Suppression du compte** : /api/users/unsubscription/{id}

### Technologies
#### Web
Le projet est écrit en C# pour la partie Web, avec la plateforme .NET 5.

### Base de données
La base de données est un base **PostgreSQL** qui est hébergée sur un serveur Alwaysdata.

Le lien entre la partie Web et la base de données se fait avec le package **Entity Framework**, qui peut gérer le ***code first*** ou ***database first***. Dans un soucis de simplicité, il est préférable d'utiliser le ***database first***.

Dans un premier temps, il est nécessaire d'initialiser la chaîne de connection à la base de données. Celle-ci se trouve dans le fichier de configuration ***appsettings.json*** et est passée au context dans le fichier ***startup.cs***.

**Important :** L'instanciation du context doit se faire avec l'annotation ***\[FromServices\]*** en paramètre d'une méthode dans un contrôleur. S'il s'agit d'un service, il faut faire une injection de dépendance.

Voici la commande d'initialisation des chaînes de connection :
> dotnet user-secrets set ConnectionStrings:GoodfoodContext "Host=postgresql-arganpiquet.alwaysdata.net;Database=arganpiquet_goodfood;Username=arganpiquet;Password=xYKvD26Mhr5CbhFv" 

Dans un second temps, il faut créer (ou modifier) la base de données et ensuite regénérer les entités avec la commande suivante :
> dotnet ef dbcontext scaffold Name=ConnectionStrings:GoodfoodContext Npgsql.EntityFrameworkCore.PostgreSQL --force --context GoodfoodContext --output-dir Models

A noter que le flag ***--force*** permet de regénérer les entités déjà créées.

Le flag ***--output-dir*** permet d'écrire les entités dans un dossier spécifique.

## Projet
### Installation
Pour installer le projet .NET sur son poste, il suffit d'effectuer un clone du repository Gitlab.

**Important** : La solution est composée de deux projets correspondant à l'application, ainsi qu'aux tests unitaires. (Se référer à la partie sur les [Tests unitaires](#unit-tests)).

Avant d'exécuter les commandes suivantes, pensez à vous positionner sur le projet **Goodfood**.

Ensuite, exécuter la commande suivante :
> dotnet build

Cette commande va permettre de regénérer tous les packages qui sont exclus du repository. Ceux-ci sont en effet inclus dans le répertoire ***bin***.

### Exécution
Afin d'exécuter le projet, lancer la commande suivante :
> dotnet run

### Déploiement
Cette partie sera écrite prochainement.

## [Tests unitaires](#unit-tests)
La solution Goodfood est composée de deux projet.
* Le projet **Goodfood** correspond à l'API 
* Le projet **Goodfood.Tests** est dédié aux tests unitaires du projet Goodfood

Les tests unitaires permettent d'automatiser les tests du projet Goodfood.

Cela consiste en l'écriture de tests (il s'agit de classes et de méthodes) qui sont exécutés par le biais d'une commande.

### Commandes d'exécution des tests
Voici les différentes commandes permettant d'exécuter les tests unitaires :

> dotnet test

Cette commande permet de lancer tous les tests.

Dans le projet Goodfood, les tests doivent être exécutés selon un ordre précis.

En effet, on ne peut pas tester l'authentification d'un utilisateur avant d'avoir testé son inscription.

> dotnet test --filter=Namespace.Classe

Cette commande permet d'exécuter tous les tests d'une classe spécifique

> dotnet test --filter=Namespace.Classe.Methode

Cette commande permet d'exécuter un seul test se trouvant dans une méthode d'une classe spécifique

## Sonarqube
Dans un soucis de qualité, de maintenabilité du code ainsi que de sécurité de l'application, il est recommandé d'analyser son code source.

Pour cela, il existe divers outils permettant de le faire, c'est le cas de **Sonarqube**.

Il s'agit en effet d'un logiciel permettant de scanner et d'analyser le code source d'applications, sites web et tout autres programmes.

Ce logiciel est écrit en Java et se lance avec la JVM.

Pour plus d'informations, merci de consulter la [documentation officielle](https://docs.sonarqube.org/latest/).

### Lancement de l'analyse
Voici les différentes commandes permettant de lancer le scan d'un projet .NET.

**Important** : le prérequis pour cette partie est d'avoir installé Sonarqube et de l'avoir lancé avec la commande suivante :
> lien/vers/répertoire/sonarqube/Sonarqube.bat

* Installation du package .NET pour Sonarqube : 
> dotnet tool install --global dotnet-sonarscanner

* Démarrage du scanner :
> dotnet sonarscanner begin /k:"goodfood-csharp" /d:sonar.host.url="http://localhost:9000"  /d:sonar.login="5b71417afdd202e83293578326f451b4370061e7"

* Lancement du build :
> dotnet build

* Arrêt du scanner :
> dotnet sonarscanner end /d:sonar.login="5b71417afdd202e83293578326f451b4370061e7"
