using csharp_goodfood_users.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace Goodfood.Tests.Core
{
    /// <summary>
    /// Class BaseTest
    /// Cette classe regroupe des éléments utilisés pour plusieurs classes de test
    /// Pour accéder à ces méthodes, la classe de test doit hériter de BaseTest
    /// </summary>
    public abstract class BaseTest
    {
        /// <summary>
        /// Récupération de l'optionsBuilder
        /// </summary>
        /// <typeparam name="GoodfoodContext">DbContext</typeparam>
        /// <returns>DbContextOptionsBuilder</returns>
        private static readonly DbContextOptionsBuilder<GoodfoodContext> optionsBuilder = new DbContextOptionsBuilder<GoodfoodContext>();

        /// <summary>
        /// Récupération du context lié au appsettings.test
        /// </summary>
        /// <returns>DbContext</returns>
        private static readonly GoodfoodContext context = new GoodfoodContext(optionsBuilder.UseNpgsql(InitConfiguration().GetConnectionString("GoodfoodContext")).Options);

        /// <summary>
        /// Instanciation du context
        /// </summary>
        /// <value>DbContext</value>
        public static GoodfoodContext Context { get => context; }

        /// <summary>
        /// Initialisation d'un utilisateur
        /// </summary>
        private static readonly string username = "vauchelmorgane@gmail.com";
        public static string Username { get => username; }

        /// <summary>
        /// Initialisation de son mot de passe
        /// </summary>
        private static string password = "!#MySecurePwd123!";
        public static string Passsword 
        { 
            get => password;
            set
            {
                Passsword = password;
            }
        }

        private static readonly string identifiant = "869de3a8-210e-4ff2-bd6a-e2f3d7ad0ffb";
        public static string Identifiant { get => identifiant; }


        /// <summary>
        /// Récupération du fichier de configuration de test
        /// </summary>
        /// <returns>IConfigurationRoot</returns>
        public static IConfiguration InitConfiguration()
        {
            string jsonSettingsPath = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("Goodfood"));

            var config = new ConfigurationBuilder()
                .AddJsonFile(jsonSettingsPath + @"Goodfood\appsettings.test.json")
                .AddEnvironmentVariables() 
                .Build();
            return config;
        }
    }
}