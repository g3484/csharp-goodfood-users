using csharp_goodfood_users.Models;

namespace Goodfood.Tests.Core
{
    public class AuthenticationResult
    {
        public User User { get; set; }
        public string Jwt { get; set; }
        public string RefreshToken { get; set; }
    }
}