using csharp_goodfood_users.Controllers;
using csharp_goodfood_users.Helpers;
using csharp_goodfood_users.Models;
using csharp_goodfood_users.Services;
using Goodfood.Tests.Core;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using Xunit;

namespace Goodfood.Tests
{
    public class LoginTest : BaseTest
    {
        [Fact]
        public void AuthenticationTest()
        {
            Security security = new Security();
            SecurityController SecurityController = new SecurityController(security, Context);

            User user = new User
            {
                MailAddress = Username,
                Password = Passsword
            };

            var result = SecurityController.Login(user) as OkObjectResult;

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(result.Value);
            SettingsHelper.AddOrUpdateAppSetting<string>("appsettings.test.json", "Jwt", JsonConvert.DeserializeObject<AuthenticationResult>(json).Jwt);
        }

        [Fact]
        public void RefreshTokenTest()
        {
            Security security = new Security();
            SecurityController SecurityController = new SecurityController(security, Context);

            RefreshToken token = Context.RefreshTokens
                .Where(x => x.Username == Username)
                .OrderByDescending(x => x.Valid)
                .FirstOrDefault();

            RefreshToken refreshToken = new RefreshToken
            {
                Token = token.Token,
                Username = Username,
                Valid = DateTime.Now
            };

            var result = SecurityController.RefreshToken(refreshToken) as ObjectResult;
        }
    }
}