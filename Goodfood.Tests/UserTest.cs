using csharp_goodfood_users.Controllers;
using csharp_goodfood_users.Models;
using csharp_goodfood_users.Services;
using Goodfood.Tests.Core;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Goodfood.Tests
{
    public class UserTest : BaseTest
    {
        [Fact]
        public void GetUserTest()
        {
            Security security = new Security();
            UserController userController = new UserController(Context, security);
            User user = new User
            {
                Id = Identifiant
            };
            var result = userController.GetUser(user.Id) as ObjectResult;
        }

        [Fact]
        public void UnsubscriptionTest()
        {
            Security security = new Security();
            UserController userController = new UserController(Context, security);
            User user = new User
            {
                Id = Identifiant,
                Firstname = "John",
                Lastname = "Doe",
                MailAddress = Username,
                Password = Passsword
            };
            var result = userController.Unsubscription(user.Id, user);
        }
    }
}