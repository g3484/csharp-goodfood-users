using csharp_goodfood_users.Controllers;
using csharp_goodfood_users.Models;
using csharp_goodfood_users.Models.ViewModels;
using csharp_goodfood_users.Services;
using Goodfood.Tests.Core;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Goodfood.Tests
{
    public class RegistrationTest : BaseTest
    {        
        [Fact]
        public void SaveUserTest()
        {
            var dbOption = new DbContextOptionsBuilder<GoodfoodContext>()
                .UseNpgsql("Host=postgresql-arganpiquet.alwaysdata.net;Database=arganpiquet_goodfood_test;Username=arganpiquet;Password=xYKvD26Mhr5CbhFv")
                .Options;
            var context = new GoodfoodContext(dbOption);
            var security = new Security();
            var registration = new Registration(context, security);
            var mail = new Mail(InitConfiguration());
            var config = InitConfiguration();

            var controller = new RegistrationController(
                config, 
                registration, 
                mail, 
                Context
            );

            HashSalt encoded = security.EncodePassword(Passsword);
            UserViewModel UVM = new UserViewModel();
            UVM.User.Firstname = "John";
            UVM.User.Lastname = "Doe";
            UVM.User.MailAddress = Username;
            UVM.User.Password = Passsword;
            UVM.Address.Lane = "2, place du Général de Gaulle";
            UVM.Address.ZipCode = "76000";
            UVM.Address.City = "Rouen";

            var result = controller.Registration(UVM);
        }
    }
}