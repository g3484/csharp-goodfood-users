using csharp_goodfood_users.Extensions;
using csharp_goodfood_users.Helpers;
using csharp_goodfood_users.Models;
using csharp_goodfood_users.Models.ViewModels;
using csharp_goodfood_users.Services;
using csharp_goodfood_users.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace csharp_goodfood_users.Controllers
{
    /// <summary>
    /// Class UserController
    /// Regroupe les actions relatives aux utilisateurs
    /// </summary>
    [ApiController]
    //[Authorize]
    [Route("api/users/")]
    public class UserController : ControllerBase
    {
        private readonly GoodfoodContext context;
        private readonly SecurityService securityService;
        private readonly UserService userService;

        /// <summary>
        /// Constructeur
        /// Initialisation des services
        /// </summary>
        /// <param name="context">DbContext</param>
        /// <param name="security">Security</param>
        public UserController(GoodfoodContext context, SecurityService securityService, UserService userService)
        {
            this.context = context;
            this.securityService = securityService;
            this.userService = userService;
        }

        /// <summary>
        /// Récupération des données d'un utilisateur par son id
        /// Affichage des données dans la page de profil
        /// </summary>
        /// <param name="id">id de l'utilisateur</param>
        /// <param name="Context">DbContext</param>
        /// <returns>User</returns>
        [HttpGet]
        [Route("profile/{id}")]
        public IActionResult GetUser(string id)
        {
            try
            {
                User user = context.Users.SingleOrDefault(x => x.Id == id);
                Address address = context.Addresses
                    .SingleOrDefault(x => x.UserId == id && x.Default == true);
                
                if (user == null)
                {
                    return NotFound(new { message = ValidationMessage.Error.UserNotFound });
                }

                List<string> roles = new List<string>();

                foreach (var role in user.Roles)
                {
                    roles.Add(role.Trim());
                }
                user.Roles = roles.ToArray();

                UserViewModel UVM = new UserViewModel
                {
                    User = user.TrimStringProperties(),
                    Address = address.TrimStringProperties()
                };
                return Ok(UVM);
            }
            catch (Exception e)
            {
                throw new ArgumentNullException("Unable to get user.", e);
            }
        }

        /// <summary>
        /// Enregistrement d'une photo de profil
        /// </summary>
        /// <param name="id">id de l'utilisateur</param>
        /// <param name="file">image</param>
        /// <param name="Context">DbContext</param>
        /// <returns>Réponse HTTP</returns>
        [HttpPatch]
        [Route("avatar/{id}")]
        public IActionResult Avatar(string id, [FromQuery] IFormFile file)
        {
            if (file.Length == 0)
            {
                return BadRequest(new { message = ValidationMessage.Error.UploadFileErrorMessage });
            }

            try
            {
                User user = context.Users.SingleOrDefault(x => x.Id == id);
                
                if (user == null)
                {
                    return NotFound(new { message = ValidationMessage.Error.UserNotFound });
                }

                user.Avatar = file.FileName;
                context.SaveChanges();
            }
            catch (Exception e) 
            {
                throw new DbUpdateException("Unable to save filename in database.", e);
            }

            try
            {
                string filePath = Path.Combine(@"Content\Uploads", file.FileName);
                using (Stream stream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }
            catch (Exception e)
            {
                throw new IOException("Unable to save file.", e);
            }
            return Ok(new { message = ValidationMessage.Success.FileUploadedSuccessfully });
        }

        /// <summary>
        /// Modification du profil de l'utilisateur
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="userBody">body</param>
        /// <returns>Réponse JSON</returns>
        [HttpPatch]
        [Route("profile/edit/{id}")]
        public IActionResult UpdateProfile(string id, [FromBody] User userBody)
        {
            User user = context.Users.SingleOrDefault(x => x.Id == id);
                
            if (user == null)
            {
                return NotFound(new { message = ValidationMessage.Error.UserNotFound });
            }

            if (String.IsNullOrWhiteSpace(userBody.Firstname) || String.IsNullOrWhiteSpace(userBody.Lastname) || String.IsNullOrWhiteSpace(userBody.Phone))
            {
                return BadRequest(new { message = ValidationMessage.Error.FormErrors });
            }

            userService.EditProfile(id, userBody);

            return Ok(new { message = ValidationMessage.Success.ProfileUpdatedSuccessfully });
        }

        /// <summary>
        /// Suppression d'un compte utilisateur
        /// </summary>
        /// <param name="id">id de l'utilisateur</param>
        /// <param name="user">Objet User</param>
        /// <param name="Context">DbContext</param>
        /// <returns>Retour JSON</returns>
        [HttpPost]
        [Route("unsubscription/{id}")]
        public IActionResult Unsubscription(string id, [FromBody] User user)
        {
            User savedUser = context.Users.SingleOrDefault(x => x.Id == id);

            if (savedUser == null)
            {
                return Unauthorized(new { message = ValidationMessage.Error.UserNotFound });
            }

            if (!securityService.IsPasswordValid(user.Password.Trim(), savedUser.Salt, savedUser.Password.Trim()))
            {
                return Unauthorized(new { message = ValidationMessage.Error.InvalidPassword });
            }

            userService.DeleteUser(id);

            return Ok(new { message = ValidationMessage.Success.UnsubscriptionSucceeded });
        }
    }
}