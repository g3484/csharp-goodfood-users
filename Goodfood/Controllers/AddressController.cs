using csharp_goodfood_users.Helpers;
using csharp_goodfood_users.Models;
using csharp_goodfood_users.Services;
using csharp_goodfood_users.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp_goodfood_users.Controllers
{
    /// <summary>
    /// Controller AddressController
    /// Encapsule toutes les méthodes concernant la gestion des adresses
    /// </summary>
    [ApiController]
    [Route("api/users/address/")]
    [Authorize]
    public class AddressController : ControllerBase
    {
        private readonly GoodfoodContext context;
        private readonly AddressService addressService;

        /// <summary>
        /// Controller
        /// </summary>
        /// <param name="context">DbContext</param>
        /// <param name="addressService">Adresse service</param>
        public AddressController(GoodfoodContext context, AddressService addressService)
        {
            this.context = context;
            this.addressService = addressService;
        }

        /// <summary>
        /// Récupération de la liste des adresses d'un utilisateur
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        /// <returns>Liste des adresses</returns>
        [HttpGet]
        [Route("all/{id}")]
        public IActionResult GetAddresses(string id)
        {
            User user = context.Users.FirstOrDefault(x => x.Id == id);

            if (user == null)
            {
                return NotFound(new { message = ValidationMessage.Error.UserNotFound });
            }
            return Ok(addressService.GetAddresses(id));
        }

        /// <summary>
        /// Ajout d'une adresse
        /// </summary>
        /// <param name="addressBody">Body</param>
        /// <returns>Réponse JSON</returns>
        [HttpPost]
        [Route("add")]
        public IActionResult AddAddress([FromBody] Address addressBody)
        {
            User user = context.Users.FirstOrDefault(x => x.Id == addressBody.UserId);

            if (user == null)
            {
                return NotFound(new { message = ValidationMessage.Error.UserNotFound });
            }

            if (EntityValidator.IsNull(addressBody))
            {
                return BadRequest(new { message = ValidationMessage.Error.FormErrors });
            }
            addressService.AddAddress(addressBody);

            return Ok(new { message = ValidationMessage.Success.AddressAddedSuccessfully });
        }

        /// <summary>
        /// Suppression d'une adresse
        /// </summary>
        /// <param name="id">Id de l'adresse</param>
        /// <returns>Réponse JSON</returns>
        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult DeleteAddress(int id)
        {
            Address address = context.Addresses.FirstOrDefault(x => x.Id == id);

            if (address == null)
            {
                return NotFound(new { message = ValidationMessage.Error.AddressNotFound });
            }

            addressService.DeleteAddress(id);

            return new ObjectResult(new { message = ValidationMessage.Success.AddressRemovedSuccessfully })
            {
                StatusCode = StatusCodes.Status204NoContent
            };
        }
    }
}