using csharp_goodfood_users.Models;
using csharp_goodfood_users.Services;
using csharp_goodfood_users.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp_goodfood_users.Controllers
{
    /// <summary>
    /// Class ResetPasswordController
    /// Ce contrôleur regroupe les méthodes concernant la réinitialisation
    /// du mot de passe utilisateur
    /// </summary>
    [ApiController]
    [Route("api/users/password/")]
    public class ResetPasswordController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly GoodfoodContext context;
        private readonly SecurityService securityService;
        private readonly MailService mailService;

        /// <summary>
        /// Constructeur
        /// Initialisation des services
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <param name="context">DbContext</param>
        /// <param name="securityService">Security</param>
        /// <param name="mailService">mailService</param>
        public ResetPasswordController(IConfiguration configuration, GoodfoodContext context, SecurityService securityService, MailService mailService)
        {
            this.configuration = configuration;
            this.context = context;
            this.securityService = securityService;
            this.mailService = mailService;
        }

        /// <summary>
        /// Enregistrement de la requête de réinitialisation du mot de passe
        /// </summary>
        /// <param name="userBody">Body de la requête POST</param>
        /// <returns>Code HTTP</returns>
        [HttpPost]
        [Route("reset")]
        public IActionResult ResetPassword([FromBody] User userBody)
        {
            User user = context.Users.SingleOrDefault(x => x.MailAddress.Trim() == userBody.MailAddress.Trim());

            if (user == null)
            {
                return Unauthorized(new { message = ValidationMessage.Error.UserNotFound });
            }

            try
            {
                ResetPassword resetPassword = new ResetPassword();
                resetPassword.UserId = user.Id;

                string resetToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                resetPassword.Token = resetToken;
                resetPassword.RequestedAt = DateTime.Now;

                int validationTime  = 2;
                resetPassword.ExpiresAt = DateTime.Now.AddHours(validationTime);

                context.Add(resetPassword);
                context.SaveChanges();

                mailService.Send(
                    user.MailAddress,
                    "Réinitialisation du mot de passe",
                    MailType.ResetPassword,
                    new Dictionary<string, string>
                    {
                        { "[Firstname]", user.Firstname },
                        { "[ValidationTime]", validationTime.ToString() },
                        { "[Url]", this.configuration.GetValue<string>("Project:WebUrl") },
                        { "[Token]", resetToken }
                    }
                );

                return new ObjectResult(ValidationMessage.Success.ResetPasswordRequestSent)
                {
                    StatusCode = StatusCodes.Status201Created,
                };
            }
            catch(Exception e)
            {
                throw new DbUpdateException("Unable to save reset password request.", e);
            }
        }

        /// <summary>
        /// Vérification du token de réinitialisation du mdp
        /// </summary>
        /// <param name="resetPassword">Objet ResetPassword</param>
        /// <returns>Code HTTP</returns>
        [HttpPost]
        [Route("check-token")]
        public IActionResult CheckToken([FromBody] ResetPassword resetPassword)
        {
            ResetPassword rp = context.ResetPasswords
                .Where(x => x.UserId == resetPassword.UserId)
                .OrderByDescending(x => x.RequestedAt)
                .FirstOrDefault();

            if (rp == null || (rp.Token.Trim() != resetPassword.Token.Trim()))
            {
                return Unauthorized(new { message = ValidationMessage.Error.InvalidResetPasswordRequest });
            }

            if (rp.ExpiresAt <= DateTime.Now)
            {
                return Unauthorized(new { ValidationMessage.Error.ExpiredRequestPasswordToken });
            }
            return Ok(new { id = rp.UserId });
        }

        /// <summary>
        /// Changement du mot de passe utilisateur après réinitialisation de celui-ci
        /// </summary>
        /// <param name="userBody">User</param>
        /// <returns>Code HTTP</returns>
        [HttpPost]
        [Route("change-password")]
        public IActionResult ChangePassword([FromBody] User userBody)
        {
            User user = context.Users.FirstOrDefault(x => x.Id == userBody.Id);

            if (user == null)
            {
                return Unauthorized(new { message = ValidationMessage.Error.UserNotFound });
            }

            try
            {
                HashSalt encoded = securityService.EncodePassword(user.Password);
                user.Password = encoded.Hash;
                user.Salt = encoded.Salt;

                context.SaveChanges();

                return Ok(new { message = ValidationMessage.Success.PasswordUpdatedSuccessfully });
            }
            catch (Exception e)
            {
                throw new DbUpdateException("An error occurred during setting new password.", e);
            }
        }
    }
}