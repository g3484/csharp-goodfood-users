using csharp_goodfood_users.Models;
using csharp_goodfood_users.Models.ViewModels;
using csharp_goodfood_users.Services;
using csharp_goodfood_users.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp_goodfood_users.Controllers
{
    /// <summary>
    /// Class RegistrationController
    /// Permet l'enregistrement d'un utilisateur
    /// </summary>
    [ApiController]
    [Route("api/users/registration")]
    public class RegistrationController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly RegistrationService registrationService;
        private readonly MailService mailService;
        private readonly GoodfoodContext context;

        /// <summary>
        /// Constructeur
        /// Initialisation des services
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <param name="registrationService">registrationService</param>
        /// <param name="mailService">Mail</param>
        /// <param name="context">DbContext</param>
        public RegistrationController(IConfiguration configuration, RegistrationService registrationService, MailService mailService, GoodfoodContext context)
        {
            this.configuration = configuration;
            this.registrationService = registrationService;
            this.mailService = mailService;
            this.context = context;
        }

        /// <summary>
        /// Inscription d'un utilisateur
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="Context">DbContext</param>
        /// <returns>Code HTTP</returns>
        [HttpPost]
        public IActionResult Registration([FromBody] UserViewModel UVM)
        {
            if (EntityValidator.IsNull(UVM.User) || EntityValidator.IsNull(UVM.Address))
            {
                return BadRequest(new { message = ValidationMessage.Error.FormErrors });   
            }

            if (IsMailAddressExists(UVM.User.MailAddress, context))
            {
                return BadRequest(new { message = ValidationMessage.Error.NotUniqueMailAddressErrorMessage });
            }

            User user = registrationService.SaveUser(UVM.User);

            UVM.Address.UserId = user.Id;
            UVM.Address.Default = true;
            registrationService.SaveAddress(UVM.Address);
            
            mailService.Send(
                UVM.User.MailAddress, 
                "Confirmation de votre compte",
                MailType.Registration,
                new Dictionary<string, string>
                {
                    { "[Firstname]", user.Firstname },
                    { "[MailAddress]", user.MailAddress },
                    { "[Url]", this.configuration.GetValue<string>("Project:WebUrl") },
                    { "[Token]", user.ValidationToken }
                });
            
            return new ObjectResult(UVM)
            {
                StatusCode = StatusCodes.Status201Created
            };
        }

        /// <summary>
        /// Permet de vérifier que l'adresse e-mail de l'utilisateur est unique
        /// </summary>
        /// <param name="mail">Adresse e-mail de l'utilisateur</param>
        /// <param name="Context">DbContext</param>
        /// <returns>bool</returns>
        private static bool IsMailAddressExists(string mail, [FromServices] GoodfoodContext Context)
        {
            try
            {
                List<User> users = Context.Users.Where(x => x.MailAddress == mail).ToList();
                return users.Any();
            }
            catch (Exception e)
            {
                throw new ArgumentNullException("Unable to get list of users.", e);
            }
        }
    }
}