using System.Net.Mail;
using csharp_goodfood_users.Helpers;
using csharp_goodfood_users.Models;
using csharp_goodfood_users.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace csharp_goodfood_users.Controllers
{
    /// <summary>
    /// SearchController
    /// Encapsule les méthodes concernant la recherche des utilisateurs
    /// </summary>
    [ApiController]
    [Route("api/users/search/")]
    [Authorize]
    public class SearchController : ControllerBase
    {
        private readonly GoodfoodContext context;
        private readonly SearchService searchService; 

        /// <summary>
        /// Constructeur
        /// Initialisation des services
        /// </summary>
        /// <param name="context">DbContext</param>
        /// <param name="searchService">SearchService</param>
        public SearchController(GoodfoodContext context, SearchService searchService)
        {
            this.context = context;
            this.searchService = searchService;
        }

        /// <summary>
        /// Rechercher un utilisateur par son id
        /// </summary>
        /// <param name="id">Id de l'utilisateur</param>
        /// <returns>Utilisateur</returns>
        [HttpGet]
        [Route("by-id/{id}")]
        public IActionResult FindById(string id)
        {
            User user = searchService.FindById(id.Trim());

            if (user == null)
            {
                return NotFound(new { message = "L'utilisateur n'existe pas." });
            }
            return Ok(user);
        }

        /// <summary>
        /// Rechercher un utilisateur par son adresse e-mail
        /// </summary>
        /// <param name="mailAddress">Adresse e-mail de l'utilisateur</param>
        /// <returns>Utilisateur</returns>
        [HttpGet]
        [Route("by-mail/{mailAddress}")]
        public IActionResult FindByMail(string mailAddress)
        {
            User user = searchService.FindByMail(mailAddress.Trim());

            if (user == null)
            {
                return NotFound(new { message = "L'utilisateur n'existe pas." });   
            }
            return Ok(user);
        }
    }
}