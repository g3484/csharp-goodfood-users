using csharp_goodfood_users.Helpers;
using csharp_goodfood_users.Models;
using csharp_goodfood_users.Services;
using csharp_goodfood_users.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp_goodfood_users.Controllers
{
    /// <summary>
    /// Controller AdminController
    /// Encapsule toutes les méthodes de l'espace administration
    /// </summary>
    [ApiController]
    [Route("api/users/admin/")]
    [Authorize("ROLE_ADMIN")]
    public class AdminController : ControllerBase
    {
        private readonly GoodfoodContext context;
        private readonly RolesService rolesService;
        private readonly UserService userService;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="userService">Service user</param>
        public AdminController(RolesService rolesService, UserService userService, GoodfoodContext context)
        {
            this.rolesService = rolesService;
            this.userService = userService;
            this.context = context;
        }

        /// <summary>
        /// Récupération de tous les utilisateurs
        /// </summary>
        /// <returns>Liste des utilisateurs</returns>
        [HttpGet]
        [Route("users")]
        public IActionResult Users()
        {
            return Ok(userService.GetUsers());
        }

        [HttpGet]
        [Route("roles/all")]
        public IActionResult GetRoles()
        {
            return Ok(rolesService.GetRoles());
        }

        /// <summary>
        /// Ajout d'un rôle
        /// </summary>
        /// <param name="role">Rôle</param>
        /// <returns>Rôle</returns>
        [HttpPost]
        [Route("roles/add")]
        public IActionResult AddRole([FromBody] Role role)
        {
            if (String.IsNullOrWhiteSpace(role.Name) || String.IsNullOrWhiteSpace(role.Value))
            {
                return BadRequest(new { message = ValidationMessage.Error.FormErrors });
            }

            rolesService.AddRole(role);

            return Ok(role);
        }

        /// <summary>
        /// Modification d'un rôle
        /// </summary>
        /// <param name="roleBody">Body</param>
        /// <param name="id">Id du rôle</param>
        /// <returns>Réponse JSON</returns>
        [HttpPut]
        [Route("roles/edit/{id}")]
        public IActionResult EditRole([FromBody] Role roleBody, int id)
        {
            Role role = context.Roles.FirstOrDefault(x => x.Id == id);

            if (role == null)
            {
                return NotFound(new { message = ValidationMessage.Error.RoleNotFound });
            }

            rolesService.EditRole(roleBody, id);
            return Ok(new { message = ValidationMessage.Success.RoleUpdatedSuccessfully });
        }

        /// <summary>
        /// Suppression d'un rôle
        /// </summary>
        /// <param name="id">Id du rôle</param>
        /// <returns>Réponse JSON</returns>
        [HttpDelete]
        [Route("roles/delete/{id}")]
        public IActionResult DeleteRole(int id)
        {
            Role role = context.Roles.FirstOrDefault(x => x.Id == id);

            if (role == null)
            {
                return NotFound(new { message = ValidationMessage.Error.RoleNotFound });
            }
            
            rolesService.DeleteRole(id);

            return new ObjectResult(new { message = ValidationMessage.Success.RoleRemovedSuccessfully })
            {
                StatusCode = StatusCodes.Status204NoContent
            };
        }

        /// <summary>
        /// Mise à jour des rôles de l'utilisateur
        /// </summary>
        /// <param name="id">Id de l'utilisateur</param>
        /// <param name="roles">Rôles à attribuer</param>
        /// <returns>Réponse JSON</returns>
        [HttpPatch]
        [Route("edit-role/{id}")]
        public IActionResult EditUserRole(string id, [FromBody] List<Role> roles)
        {
            User user = context.Users.FirstOrDefault(x => x.Id == id);

            if (user == null)
            {
                return NotFound(new { message = ValidationMessage.Error.UserNotFound });
            }

            userService.UpdateRoles(id, roles);
            
            return Ok(new { message = ValidationMessage.Success.RoleUpdatedSuccessfully });
        }

        /// <summary>
        /// Suppression d'un utilisateur
        /// </summary>
        /// <param name="id">Id de l'utilisateur</param>
        /// <returns>Réponse JSON</returns>
        [HttpDelete]
        [Route("delete-user/{id}")]
        public IActionResult DeleteUser(string id)
        {
            User user = context.Users.FirstOrDefault(x => x.Id == id);

            if (user == null)
            {
                return NotFound(new { message = ValidationMessage.Error.UserNotFound });
            }

            userService.DeleteUser(id);

            return new ObjectResult(new { message = ValidationMessage.Success.UserAccountRemovedSuccessfully })
            {
                StatusCode = StatusCodes.Status204NoContent
            };
        }
    }
}