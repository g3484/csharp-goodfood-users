using System.Collections.Generic;
using csharp_goodfood_users.Extensions;
using csharp_goodfood_users.Helpers;
using csharp_goodfood_users.Models;
using csharp_goodfood_users.Services;
using csharp_goodfood_users.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Globalization;
using System.Linq;

namespace csharp_goodfood_users.Controllers
{
    /// <summary>
    /// Class SecurityController
    /// Regroupe les actions relatives à la sécurité
    /// </summary>
    [ApiController]
    [Route("api/users/")]
    public class SecurityController : ControllerBase
    { 
        private readonly SecurityService securityService;
        private readonly UserService userService;
        private readonly GoodfoodContext context;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="securityService">Service security</param>
        /// <param name="context">DbContext</param>
        public SecurityController(UserService userService, SecurityService securityService, GoodfoodContext context)
        {
            this.userService = userService;
            this.securityService = securityService;
            this.context = context;
        }

        /// <summary>
        /// Cette méthode permet aux utilisateurs de se connecter à leur compte.
        /// L'authentification se fait lorsque l'utilisateur saisit son adresse e-mail et son mot de passe
        /// Si les identifiants sont valides, le JWT est renvoyé
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="Context">DbContext</param>
        /// <returns>Réponse Json</returns>
        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] User user)
        {
            try
            {
                User input = context.Users.FirstOrDefault(u => u.MailAddress.Trim() == user.MailAddress);

                if (input == null)
                {
                    return Unauthorized(new { message = ValidationMessage.Error.InvalidCredentials });
                }

                bool isPasswordValid = securityService.IsPasswordValid(user.Password.Trim(), input.Salt, input.Password.Trim());

                if (!isPasswordValid)
                {
                    return Unauthorized(new { message = ValidationMessage.Error.InvalidCredentials });
                }
                
                RefreshToken refreshToken = new RefreshToken();
                string secretKey = "cp7+JNduTOq9RaoYgMqaiqLGFJ22tlsRdbOQ7/b3TCOskkGvHs13wDom8Zqx+1vaZpYK1uAl4U5SV3togoJbUQ==";
                refreshToken.Token = secretKey;
                refreshToken.Username = user.MailAddress;
                refreshToken.Valid = DateTime.Now;
                context.Add(refreshToken);
                context.SaveChanges();

                List<string> roles = new List<string>();

                foreach (var role in input.Roles)
                {
                    roles.Add(role.Trim());
                }
                input.Roles = roles.ToArray();

                return Ok(new 
                {
                    user = input.TrimStringProperties(),
                    jwt = securityService.GenerateJwtToken(input),
                    refreshToken = secretKey
                });
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Unable to login.", e);
            }
        }

        /// <summary>
        /// Rafraichissement du JWT
        /// </summary>
        /// <param name="token">token</param>
        /// <returns>JWT</returns>
        [HttpPost]
        [Route("refresh_token")]
        public IActionResult RefreshToken([FromBody] RefreshToken token)
        {
            try
            {
                User user = context.Users.FirstOrDefault(x => x.MailAddress == token.Username);

                if (user == null)
                {
                    return Unauthorized(new { message = ValidationMessage.Error.UserNotFound });
                }

                RefreshToken storedToken = context.RefreshTokens
                    .Where(x => x.Username == token.Username)
                    .OrderByDescending(x => x.Valid)
                    .FirstOrDefault();

                if (storedToken.Token.Trim() != token.Token.Trim())
                {
                    return Unauthorized(new { message = ValidationMessage.Error.TokensDismatch });
                }

                string jwt = securityService.GenerateJwtToken(user);

                return Ok(new { jwt = jwt, RefreshToken = storedToken.Token.Trim() });
            } catch (Exception e)
            {
                throw new ArgumentNullException("An error occurred during refresh JWT.", e);
            }
        }

        /// <summary>
        /// Validation du compte
        /// </summary>
        /// <param name="mail">Adresse e-maim</param>
        /// <param name="token">Token</param>
        /// <returns>Code HTTP</returns>
        [HttpGet]
        [Route("confirm/{mail}/{token}")]
        public IActionResult AccountValidation(string mail, string token)
        {
            User user = context.Users.SingleOrDefault(x => x.MailAddress == mail.Trim());

            if (user == null)
            {
                return Unauthorized(new { message = ValidationMessage.Error.UserNotFound });
            }

            if (user.ValidationToken.Trim() != token.Trim())
            {
                return Unauthorized(new { message = ValidationMessage.Error.TokensDismatch });
            }

            try
            {
                // Le token de validation est remplacé par le mot-clé "Valid".
                user.ValidationToken = "Valid";
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Error during changing validation token.", e);
            }

            RefreshToken refreshToken = new RefreshToken();
            string secretKey = "cp7+JNduTOq9RaoYgMqaiqLGFJ22tlsRdbOQ7/b3TCOskkGvHs13wDom8Zqx+1vaZpYK1uAl4U5SV3togoJbUQ==";
            refreshToken.Token = secretKey;
            refreshToken.Username = user.MailAddress;
            refreshToken.Valid = DateTime.Now;
            context.Add(refreshToken);
            context.SaveChanges();

            return Ok(new 
            {
                jwt = securityService.GenerateJwtToken(user),
                refreshToken = secretKey
            });
        }

        /// <summary>
        /// Changement du mot de passe utilisateur
        /// </summary>
        /// <param name="changePassword">Objet ChangePassword</param>
        /// <returns>Code HTTP</returns>
        [Authorize]
        [HttpPost]
        [Route("change-password")]
        public IActionResult ChangePassword([FromBody] ChangePassword changePassword)
        {
            User user = context.Users.SingleOrDefault(x => x.Id == changePassword.Id);

            if (user == null)
            {
                return Unauthorized(new { message = ValidationMessage.Error.UserNotFound });
            }

            if (!securityService.IsPasswordValid(changePassword.OldPassword.Trim(), user.Salt, user.Password.Trim()))
            {
                return Unauthorized(new { message = ValidationMessage.Error.InvalidPassword });
            }

            try
            {
                HashSalt encoded = securityService.EncodePassword(changePassword.NewPassword);
                user.Password = encoded.Hash;
                user.Salt = encoded.Salt;

                context.SaveChanges();
            }
            catch(Exception e)
            {
                throw new DbUpdateException("An error occurred during updating user password.", e);
            }
            return Ok(new { message = ValidationMessage.Success.PasswordUpdatedSuccessfully });
        }

        /// <summary>
        /// Check K8S Health
        /// </summary>
        /// <returns>Code HTTP</returns>
        [HttpGet]
        [Route("health")]
        public IActionResult Health() => Ok(new { message = "K8S is healthy" });
    }
}