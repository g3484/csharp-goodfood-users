﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharp_goodfood_users.Models
{
    public partial class RefreshToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string Username { get; set; }
        public DateTime Valid { get; set; }
    }
}
