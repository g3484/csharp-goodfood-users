﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharp_goodfood_users.Models
{
    public partial class Address
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Lane { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public bool? Default { get; set; }
    }
}
