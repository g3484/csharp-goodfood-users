namespace csharp_goodfood_users.Models
{
    public static class Roles
    {
        /// <summary>
        /// Rôle client
        /// </summary>
        private static readonly string client = "ROLE_CLIENT";

        /// <summary>
        /// Rôle livreur 
        /// </summary>
        private static readonly string deliverer = "ROLE_DELIVERER";

        /// <summary>
        /// Rôle comptable
        /// </summary>
        private static readonly string accountant = "ROLE_ACCOUNTANT";

        /// <summary>
        /// Rôle administrateur
        /// </summary>
        private static readonly string admin = "ROLE_ADMIN";

        public static string Client { get => Roles.client; }
        public static string Deliverer { get => Roles.deliverer; }
        public static string Accountant { get => Roles.Accountant; }
        public static string Admin { get => Roles.Admin; }
    }
}