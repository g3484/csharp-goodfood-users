﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharp_goodfood_users.Models
{
    public partial class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
