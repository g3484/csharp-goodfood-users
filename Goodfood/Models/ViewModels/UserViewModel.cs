namespace csharp_goodfood_users.Models.ViewModels
{
    public class UserViewModel
    {
        public User User { get; set; }
        public Address Address { get; set; }
    }
}