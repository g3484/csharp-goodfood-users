﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharp_goodfood_users.Models
{
    public partial class ResetPassword
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Token { get; set; }
        public DateTime RequestedAt { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}
