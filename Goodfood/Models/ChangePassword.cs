namespace csharp_goodfood_users.Models
{
    public class ChangePassword
    {
        public string Id { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}