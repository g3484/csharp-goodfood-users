﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharp_goodfood_users.Models
{
    public partial class User
    {
        public string Id { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string MailAddress { get; set; }
        public string Password { get; set; }
        public string Avatar { get; set; }
        public byte[] Salt { get; set; }
        public string ValidationToken { get; set; }
        public string[] Roles { get; set; }
        public string Phone { get; set; }
    }
}
