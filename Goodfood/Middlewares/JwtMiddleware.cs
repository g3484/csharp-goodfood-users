using csharp_goodfood_users.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_goodfood_users.Middlewares
{
    /// <summary>
    /// Class JwtMiddleware
    /// Permet de vérifier la validité du JWT
    /// Cette classe est chargée dans le startup.cs
    /// Et est appelée avec une annotation Authorize
    /// </summary>
    public class JwtMiddleware
    {
        private readonly RequestDelegate requestDelegate;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="requestDelegate">RequestDelegate</param>
        public JwtMiddleware(RequestDelegate requestDelegate)
        {
            this.requestDelegate = requestDelegate;
        } 

        /// <summary>
        /// Méthode Invoke
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>Context</returns>
        public async Task Invoke(HttpContext context, GoodfoodContext goodfoodContext)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
            {
                AttachUserToContext(context, goodfoodContext, token);
            }
            await this.requestDelegate(context);
        }

        /// <summary>
        /// Récupération du User lorsque le Jwt est décodé
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <param name="token">Jwt</param>
        private static void AttachUserToContext(HttpContext context, GoodfoodContext goodfoodContext, string token)
        {
            try
            {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
                byte[] key = Encoding.ASCII.GetBytes(Key.SecretKey);

                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validateToken);

                var jwtToken = (JwtSecurityToken)validateToken;
                var mailAddress = jwtToken.Claims.First(x => x.Type == "mail").Value;

                context.Items["User"] = goodfoodContext.Users.FirstOrDefault(x => x.MailAddress == mailAddress);
            }
            catch
            {
                // Le bloc catch doit resté muet afin d'afficher le message d'erreur en retour.
                // Une exception peut être retournée lors du debugage
            }
        }
    }
}