using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace csharp_goodfood_users.Extensions
{
    /// <summary>
    /// Classe ExtensionMethods
    /// Encapsulation des extensions
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Trim sur toutes les propriétés string d'un objet
        /// </summary>
        /// <param name="input">objet</param>
        /// <typeparam name="TSelf"></typeparam>
        /// <returns>objet</returns>
        public static TSelf TrimStringProperties<TSelf>(this TSelf input)
        {
            var stringProperties = input.GetType().GetProperties()
                .Where(p => p.PropertyType == typeof(string) && p.CanWrite);

            foreach (var stringProperty in stringProperties)
            {
                string currentValue = (string)stringProperty.GetValue(input, null);
                if (currentValue != null)
                {
                    stringProperty.SetValue(input, currentValue.Trim(), null);   
                }
            }
            return input;
        }
    }
}