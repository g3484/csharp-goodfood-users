using Newtonsoft.Json;
using System;
using System.IO;

namespace csharp_goodfood_users.Helpers
{
    /// <summary>
    /// Class SettingsHelper
    /// </summary>
    public static class SettingsHelper
    {
        /// <summary>
        /// Cette méthode permet d'ajouter ou de modifier une section
        /// dans un fichier de configuration (appsettings.*.json)
        /// </summary>
        /// <param name="configurationFileName">Nom du fichier de configuration</param>
        /// <param name="section">section</param>
        /// <param name="value">valeur</param>
        /// <typeparam name="T"></typeparam>
        public static void AddOrUpdateAppSetting<T>(string configurationFileName, string section, T value)
        {
            try
            {
                string projectPath = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("Goodfood"));
                string path = Path.Combine(projectPath + "Goodfood", configurationFileName);

                if (!File.Exists(path))
                {
                    throw new FileNotFoundException("Unable to find \"" + path + "\" configuration file.");
                }

                string json = File.ReadAllText(path);
                dynamic jsonObj = JsonConvert.DeserializeObject(json);
                
                SetValueRecursively(section, jsonObj, value);

                string output = JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(path, output);
            }
            catch (Exception e)
            {
                throw new ArgumentException("Error during writing in \"" + configurationFileName + "\" file.", e);
            }
        }

        /// <summary>
        /// Ajout de la section et de sa valeur
        /// </summary>
        /// <param name="section">section</param>
        /// <param name="jsonObj">objet json</param>
        /// <param name="value">valeur</param>
        /// <typeparam name="T"></typeparam>
        private static void SetValueRecursively<T>(string section, dynamic jsonObj, T value)
        {
            string[] remainingSections = section.Split(":", 2);
            string currentSection = remainingSections[0];

            if (remainingSections.Length > 1)
            {
                string nextSection = remainingSections[1];
                SetValueRecursively(nextSection, jsonObj[currentSection], value);
            } 
            else 
            {
                jsonObj[currentSection] = value;
            }
        }
    }
}