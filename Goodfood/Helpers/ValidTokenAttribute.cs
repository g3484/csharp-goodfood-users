using csharp_goodfood_users.Models;
using csharp_goodfood_users.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace csharp_goodfood_users.Helpers
{
    /// <summary>
    /// Class ValidTokenAttribute
    /// Permet de vérifier que le compte d'un utilisateur a été validé
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ValidTokenAttribute : Attribute, IAuthorizationFilter
    {
        /// <summary>
        /// Vérification du token de validation
        /// </summary>
        /// <param name="context">Context</param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            User user = (User)context.HttpContext.Items["User"];

            if (user == null || user.ValidationToken.Trim() != "Valid")
            {
                context.Result = new JsonResult(new {
                    message = ValidationMessage.Error.InvalidToken
                }) 
                {
                    StatusCode = StatusCodes.Status401Unauthorized
                };
            }
        }
    }
}