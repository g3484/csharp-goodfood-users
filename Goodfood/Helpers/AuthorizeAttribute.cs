using csharp_goodfood_users.Models;
using csharp_goodfood_users.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Runtime.InteropServices;
using System.Reflection;

namespace csharp_goodfood_users.Helpers
{
    /// <summary>
    /// Class AuthorizeAttribute
    /// Permet de vérifier le JWT lorsqu'une méthode d'un contrôle
    /// dispose d'une annotation "Authorize"
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string Role;

        /// <summary>
        /// Constructeur
        /// Si aucun paramètre n'est défini, le JWT va être vérifié
        /// Si un paramètre est défini, le rôle de l'utilisateur sera vérifié
        /// </summary>
        /// <param name="role">paramètre rôle</param>
        public AuthorizeAttribute([Optional] string role)
        {
            this.Role = role;
        }

        /// <summary>
        /// Vérification du JWT
        /// </summary>
        /// <param name="context">Context</param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            User user = (User)context.HttpContext.Items["User"];

            // Si l'utilisateur est null (aucun JWT), un code 401 est retourné
            if (user == null)
            {
                context.Result = new JsonResult(new {
                    message = ValidationMessage.Error.InvalidJwtToken
                }) 
                {
                    StatusCode = StatusCodes.Status401Unauthorized
                };
            }
        }

        /// <summary>
        /// Vérifier si l'utilisateur connecté a un rôle ADMIN
        /// </summary>
        /// <param name="user">Objet User</param>
        /// <returns>bool</returns>
        private bool isAdmin(User user)
        {
            bool isAdmin = false;

            foreach (string role in user.Roles)
            {
                if (role.Trim() == this.Role.Trim())
                {
                    return true;
                }
            }
            return isAdmin;
        }
    }
}