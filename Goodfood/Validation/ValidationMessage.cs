using csharp_goodfood_users.Validation.Messages;

namespace csharp_goodfood_users.Validation
{
    /// <summary>
    /// Class ValidationMessage
    /// Mutualisation des messages d'erreur utilisées dans le projet
    /// </summary>
    public static class ValidationMessage
    {
        private static readonly ErrorMessage error = new ErrorMessage();
        private static readonly SuccessMessage success = new SuccessMessage();

        public static ErrorMessage Error { get => error; }

        public static SuccessMessage Success { get => success; }
    }
}