namespace csharp_goodfood_users.Validation.Messages
{
    /// <summary>
    /// Liste exhaustive des messages de succès
    /// </summary>
    public class SuccessMessage
    {
        /// <summary>
        /// L'import de fichier s'est bien déroulé
        /// </summary>
        private readonly string fileUploadedSuccessfully = "Votre avatar a bien été ajouté !";

        /// <summary>
        /// La désinscription s'est bien déroulée
        /// </summary>
        private readonly string unsubscriptionSucceeded = "Votre compte a bien été supprimé.";

        /// <summary>
        /// Changement de mot de passe réussi
        /// </summary>
        private readonly string passwordUpdatedSuccessfully = "Votre mot de passe a bien été changé !";

        /// <summary>
        /// Réinitilisation de mot de passe prise en compte
        /// </summary>
        private readonly string resetPasswordRequestSent = "Votre demande de réinitialisation de votre mot de passe a bien été prise en compte.";
        
        /// <summary>
        /// Adresse ajoutée
        /// </summary>
        private readonly string addressAddedSuccessfully = "L'adresse a bien été ajoutée !";

        /// <summary>
        /// Adresse supprimée
        /// </summary>
        private readonly string addressRemovedSuccessfully = "L'adresse a bien été supprimée !";

        /// <summary>
        /// Modification du rôle
        /// </summary>
        private readonly string roleUpdatedSuccessfully = "Le rôle a bien été modifié !";

        /// <summary>
        /// Suppression d'un rôle
        /// </summary>
        private readonly string roleRemovedSuccessfully = "Le rôle a bien été supprimé !";

        /// <summary>
        /// Suppression d'un compte utilisateur
        /// </summary>
        private readonly string userAccountRemovedSuccessfully = "L'utilisateur a bien été supprimé !";

        /// <summary>
        /// Modification du profil utilisateur
        /// </summary>
        private readonly string profileUpdatedSuccessfully = "Votre profil a bien été mis à jour !";

        public string FileUploadedSuccessfully { get => fileUploadedSuccessfully; }

        public string UnsubscriptionSucceeded { get => unsubscriptionSucceeded; }

        public string PasswordUpdatedSuccessfully { get => passwordUpdatedSuccessfully; }

        public string ResetPasswordRequestSent { get => resetPasswordRequestSent; }

        public string AddressAddedSuccessfully { get => addressAddedSuccessfully; }

        public string AddressRemovedSuccessfully { get => addressRemovedSuccessfully; }

        public string RoleUpdatedSuccessfully { get => roleUpdatedSuccessfully; }

        public string RoleRemovedSuccessfully { get => roleRemovedSuccessfully; }

        public string UserAccountRemovedSuccessfully { get => userAccountRemovedSuccessfully; }

        public string ProfileUpdatedSuccessfully { get => profileUpdatedSuccessfully; }
    }
}