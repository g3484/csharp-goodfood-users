namespace csharp_goodfood_users.Validation.Messages
{
    /// <summary>
    /// Liste exhaustive des messages d'erreur
    /// </summary>
    public class ErrorMessage
    {
        /// <summary>
        /// Adresse e-mail déjà existante
        /// </summary>
        private readonly string notUniqueMailAddressErrorMessage = "Cette adresse e-mail est déjà utilisée.";

        /// <summary>
        /// Erreurs détectées dans un formulaire
        /// </summary>
        private readonly string formErrors = "Le formulaire contient des erreurs.";

        /// <summary>
        /// L'utilisateur n'existe pas
        /// </summary>
        private readonly string userNotFound = "L'utilisateur n'existe pas.";

        /// <summary>
        /// Invalidité des tokens
        /// </summary>
        private readonly string tokensDismatch = "Les jetons ne correspondent pas.";

        /// <summary>
        /// Erreur pendant l'importation d'un fichier
        /// </summary>
        private readonly string uploadFileErrorMessage = "No file has been imported.";

        /// <summary>
        /// Identifiant et/ou mot de passe incorrect
        /// </summary>
        private readonly string invalidCredentials = "Adresse e-mail ou mot de passe incorrect.";

        /// <summary>
        /// Mot de passe incorrect
        /// </summary>
        private readonly string invalidPassword = "Le mot de passe est incorrect.";

        /// <summary>
        /// Jwt incorrect
        /// </summary>
        private readonly string invalidJwtToken = "Le JWT est invalide.";

        /// <summary>
        /// Compte non validé
        /// </summary>
        private readonly string invalidToken = "Vous devez valider votre compte pour accéder à cette page.";

        /// <summary>
        /// Demande de réinitialisation du mot de passe incorrecte
        /// La requête n'existe pas ou les tokens ne correspondent pas
        /// </summary>
        private readonly string invalidResetPasswordRequest = "La demande de réinitialisation du mot de passe a échouée. Veuillez réessayer";

        /// <summary>
        /// Le token de réinitialisation du mot de passe a expiré
        /// </summary>
        private readonly string expiredRequestPasswordToken = "Le lien de réinitialisation du mot de passe a expiré. Veuillez refaire une demande de réinitialisation de votre mot de passe";

        /// <summary>
        /// Message lorsque l'on n'est pas autorisé à accéder à une page
        /// </summary>
        private readonly string unauthorizedErrorMessage = "Vous n'êtes pas autorisé à accéder à ce contenu.";

        /// <summary>
        /// Adresse non trouvée
        /// </summary>
        private readonly string addressNotFound = "L'adresse n'existe pas.";

        /// <summary>
        /// Rôle non trouvé
        /// </summary>
        private readonly string roleNotFound = "Le rôle n'existe pas.";

        public string NotUniqueMailAddressErrorMessage { get => notUniqueMailAddressErrorMessage; }

        public string FormErrors { get => formErrors; }

        public string UserNotFound { get => userNotFound; }
        
        public string TokensDismatch { get => tokensDismatch; }

        public string UploadFileErrorMessage { get => uploadFileErrorMessage; }

        public string InvalidCredentials { get => invalidCredentials; }
        
        public string InvalidPassword { get => invalidPassword; }
        
        public string InvalidJwtToken { get => invalidJwtToken; }
        
        public string InvalidToken { get => invalidToken; }

        public string InvalidResetPasswordRequest { get => invalidResetPasswordRequest; }

        public string ExpiredRequestPasswordToken { get => expiredRequestPasswordToken; }

        public string UnauthorizedErrorMessage { get => unauthorizedErrorMessage; }

        public string AddressNotFound { get => addressNotFound; }

        public string RoleNotFound { get => roleNotFound; }
    }
}