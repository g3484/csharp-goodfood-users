using System;
using System.Linq;

namespace csharp_goodfood_users.Validation
{
    /// <summary>
    /// Class EntityValidator
    /// Permet la validation des entités
    /// </summary>
    public static class EntityValidator
    {
        /// <summary>
        /// Permet de vérifier si tous les champs d'une entité sont null
        /// </summary>
        /// <param name="entity">Entité</param>
        /// <returns>bool</returns>
        public static bool IsNull(object entity)
        {
            return entity.GetType().GetProperties()
                .Where(p => p.GetValue(entity) is string)
                .Any(p => String.IsNullOrWhiteSpace((p.GetValue(entity) as string)));
        }
    }
}