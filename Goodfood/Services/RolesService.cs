using csharp_goodfood_users.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp_goodfood_users.Services
{
    public class RolesService
    {
        private readonly GoodfoodContext context;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="context">DbContext</param>
        public RolesService(GoodfoodContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Récupération des rôles
        /// </summary>
        /// <returns>Liste des rôles</returns>
        public List<Role> GetRoles()
        {
            try
            {
                return context.Roles.ToList();
            }
            catch (Exception e)
            {
                throw new ArgumentNullException("Erreur lors de la récupération des rôles.", e);
            }
        }

        /// <summary>
        /// Ajout d'un rôle
        /// </summary>
        /// <param name="role">Rôle</param>
        public void AddRole(Role role)
        {
            try
            {
                context.Add(role);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de l'ajout d'un role.", e);
            }
        }

        /// <summary>
        /// Modification d'un rôle
        /// </summary>
        /// <param name="role">Rôle</param>
        /// <param name="id">Id du rôle</param>
        public void EditRole(Role role, int id)
        {
            try
            {
                Role entity = context.Roles.FirstOrDefault(x => x.Id == id);
                entity.Name = role.Name;
                entity.Value = role.Value;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de la modification d'un rôle.", e);
            }
        }

        /// <summary>
        /// Suppression d'un rôle
        /// </summary>
        /// <param name="id">Id du rôle</param>
        public void DeleteRole(int id)
        {
            try
            {
                Role role = context.Roles.FirstOrDefault(x => x.Id == id);
                context.Remove(role);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de la suppression d'un rôle.", e);
            }
        }
    }
}