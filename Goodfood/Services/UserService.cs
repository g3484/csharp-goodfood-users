using System.Collections;
using csharp_goodfood_users.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace csharp_goodfood_users.Services
{
    /// <summary>
    /// UserService
    /// Encapsulation de toutes les méthodes concernant les utilisateurs
    /// </summary>
    public class UserService
    {
        private readonly GoodfoodContext context;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="context">DbContext</param>
        public UserService(GoodfoodContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Récupération de la liste des utilisateurs
        /// </summary>
        /// <returns>Liste des utilisateurs</returns>
        public List<User> GetUsers() 
        {
            try
            {
                return context.Users.ToList();
            }
            catch (Exception e)
            {
                throw new ArgumentNullException("Erreur lors de la récupération des utilisateurs.", e);
            }
        }

        /// <summary>
        /// Mise à jour des rôles de l'utilisateur
        /// </summary>
        /// <param name="id">Id de l'utilisateur</param>
        /// <param name="roles">Nouveaux rôles à attribuer</param>
        public void UpdateRoles(string id, List<Role> roles)
        {
            try
            {
                User user = context.Users.FirstOrDefault(x => x.Id == id);

                List<string> rolesList = new List<string>();

                foreach (var item in roles)
                {
                    rolesList.Add(item.Value);
                }
                user.Roles = rolesList.ToArray();
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de la mise à jour des rôles de l'utilisateur.", e);
            }
        }

        /// <summary>
        /// Modification du profil de l'utilisateur
        /// </summary>
        /// <param name="id">Id de l'utilisateur</param>
        /// <param name="user">Utilisateur</param>
        public void EditProfile(string id, User user)
        {
            try
            {
                User entity = context.Users.FirstOrDefault(x => x.Id == id);
                
                entity.Firstname = user.Firstname;
                entity.Lastname = user.Lastname;
                entity.Phone = user.Phone;

                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de la modification du profil de l'utilisateur.", e);
            }
        }

        /// <summary>
        /// Suppression d'un utilisateur
        /// </summary>
        /// <param name="id">Id de l'utilisateur</param>
        public void DeleteUser(string id)
        {
            User user = context.Users.FirstOrDefault(x => x.Id == id);

            // Etape 1 : suppression des adresses de l'utilisateur
            try
            {
                List<Address> addresses = context.Addresses.Where(x => x.UserId == id).ToList();
                context.RemoveRange(addresses);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de la suppression des adresses de l'utilisateur.", e);
            }

            // Etape 2 : suppression des refresh token de l'utilisateur
            try
            {
                List<RefreshToken> refreshTokens = context.RefreshTokens.Where(x => x.Username == user.MailAddress).ToList(); 
                context.RemoveRange(refreshTokens);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de la suppression des refresh token de l'utilisateur.", e);
            }

            // Etape 3 : suppression des password tokens de l'utilisateur
            try
            {
                List<ResetPassword> passwordTokens = context.ResetPasswords.Where(x => x.UserId == id).ToList();
                context.RemoveRange(passwordTokens);
                context.SaveChanges();
            }
            catch (Exception e) 
            {
                throw new DbUpdateException("Erreur lors de la suppression des token de rafraichissement de mot de passe de l'utilisateur.", e);
            }

            // Etape 4 : suppression de l'utilisateur
            try
            {
                context.Remove(user);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de la suppression de l'utilisateur.", e);
            }
        }
    }
}