using csharp_goodfood_users.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace csharp_goodfood_users.Services
{
    /// <summary>
    /// Class Registration
    /// Cette classe encapsule les méthodes permettant d'enregistrer un utilisateur
    /// </summary>
    public class RegistrationService
    {
        private readonly GoodfoodContext context;
        private readonly SecurityService securityService;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="context">DbContext</param>
        /// <param name="securityService">Service security</param>
        public RegistrationService(GoodfoodContext context, SecurityService securityService)
        {
            this.context = context;
            this.securityService = securityService;
        }

        /// <summary>
        /// Enregistrement d'un utilisateur
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>User</returns>
        public User SaveUser(User user)
        {
            try
            {
                // Initialisation de l'id en GUID
                user.Id = Guid.NewGuid().ToString();

                // Hashage du mot de passe
                HashSalt encoded = securityService.EncodePassword(user.Password);
                user.Password = encoded.Hash;
                user.Salt = encoded.Salt;

                // Enregistrement du token de validation du compte.
                TokenService tokenService = new TokenService(60);
                user.ValidationToken = tokenService.GenerateToken();

                string[] roles = new string[1] { Roles.Client };
                user.Roles = roles;

                context.Add(user);
                context.SaveChanges();

                return user;
            }
            catch (Exception e)
            {
                throw new DbUpdateException("An error occurred during saving an user.", e);
            }
        }

        /// <summary>
        /// Enregistrement de l'adresse de domicile d'un utilisateur
        /// </summary>
        /// <param name="address">Address</param>
        public void SaveAddress(Address address)
        {
            try
            {
                context.Add(address);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("An error occurred during saving user home address.", e);
            }
        }
    }
}