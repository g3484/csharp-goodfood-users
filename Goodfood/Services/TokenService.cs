
using System.Globalization;
using System.Linq;
using System;

namespace csharp_goodfood_users.Services
{
    /// <summary>
    /// TokenService
    /// Ce service encapsule les méthodes relatives à la génération des tokens
    /// </summary>
    public class TokenService
    {
        public int length;
        private readonly string alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";

        /// <summary>
        /// Constructeur
        /// Permet d'initialiser la longueur du token
        /// </summary>
        /// <param name="length">Longueur du token</param>
        public TokenService(int length)
        {
            this.length = length;
        }

        /// <summary>
        /// Génération du token
        /// Le token est généré à partir de la string "alphabet"
        /// Celle-ci est mélangée et coupée pour obtenir la longueur voulue
        /// </summary>
        /// <returns>Token</returns>
        public string GenerateToken()
        {
            return StringRepeat(Shuffle()).Substring(0, this.length);
        }

        /// <summary>
        /// Répétition de la séquence
        /// </summary>
        /// <param name="token">Chaîne de caractères</param>
        /// <returns>Chaîne répétée</returns>
        private string StringRepeat(string token) => 
            String.Concat(Enumerable.Repeat(token, 2));
        
        /// <summary>
        /// Mélange de la chaîne de caractères
        /// </summary>
        /// <returns>Chaîne de caractères mélangée aléatoirement</returns>
        private string Shuffle()
        {
            Random num = new Random();
            string rand = new string(alphabet.ToCharArray()
                .OrderBy(s => (num.Next(2) % 2) == 0).ToArray());

            return rand;
        }
    }
}