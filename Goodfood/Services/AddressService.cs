using csharp_goodfood_users.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp_goodfood_users.Services
{
    /// <summary>
    /// AddressService
    /// Encapsulation de toutes les méthodes concernant la gestion des adresses
    /// </summary>
    public class AddressService
    {
        private readonly GoodfoodContext context;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="context">DbContext</param>
        public AddressService(GoodfoodContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Récupération des adresses de l'utilisateur
        /// </summary>
        /// <param name="id">Id de l'utilisateur</param>
        /// <returns>Liste des adresses</returns>
        public List<Address> GetAddresses(string id)
        {
            try
            {
                return context.Addresses
                    .Where(x => x.UserId == id)
                    .ToList();
            }
            catch (Exception e)
            {
                throw new ArgumentNullException("Erreur lors de la récupération des adresses de l'utilisateur.", e);
            }
        }

        /// <summary>
        /// Ajout d'une adresse
        /// </summary>
        /// <param name="address">Addresse</param>
        public void AddAddress(Address address)
        {
            try
            {
                context.Addresses.Add(address);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de l'ajout d'une adresse.", e);
            }
        }

        /// <summary>
        /// Suppression d'une adresse
        /// </summary>
        /// <param name="id">Id de l'adresse</param>
        public void DeleteAddress(int id)
        {
            try
            {
                Address address = context.Addresses.FirstOrDefault(x => x.Id == id);
                context.Remove(address);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new DbUpdateException("Erreur lors de la suppression d'une adresse.", e);
            }
        }
    }
}