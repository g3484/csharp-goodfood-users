using csharp_goodfood_users.Helpers;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System.Collections.Generic;

namespace csharp_goodfood_users.Services
{
    /// <summary>
    /// Enum des différents templates
    /// </summary>
    public enum MailType
    {
        Registration,
        ResetPassword
    }

    /// <summary>
    /// Class MailService
    /// Ce service regroupe les méthodes permettant l'envoi de mail.
    /// </summary>
    public class MailService
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Constructeur
        /// Initialisation du fichier de configuration
        /// </summary>
        /// <param name="configuration"></param>
        public MailService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        
        /// <summary>
        /// Envoi de mail
        /// </summary>
        /// <param name="to">destinataire</param>
        /// <param name="subject">objet du mail</param>
        /// <param name="template">template</param>
        /// <param name="args">arguments à passer dans le template</param>
        public void Send(string to, string subject, MailType mailType, Dictionary<string, string> args)
        {
            MimeMessage message = new MimeMessage();
            message.From.Add(new MailboxAddress(
                this.configuration.GetValue<string>("Project:Name"), 
                this.configuration.GetValue<string>("Project:MailAddress")));
            message.To.Add(InternetAddress.Parse(to));
            message.Subject = subject;

            string viewContent = MailType.Registration == mailType
                ? AccountConfirmationTemplate()
                : GetResetPasswordTemplate();

            foreach (KeyValuePair<string, string> argument in args)
            {
                viewContent = viewContent.Replace(argument.Key.Trim().ToString(), argument.Value);
            }

            message.Body = new TextPart ("html")
            {
                Text = viewContent
            };

            try
            {
                using (SmtpClient client = new SmtpClient())
                {
                    client.Connect(
                        this.configuration.GetValue<string>("Smtp:Host"), 
                        int.Parse(this.configuration.GetValue<string>("Smtp:Port")), 
                        MailKit.Security.SecureSocketOptions.SslOnConnect);
                    client.Authenticate(
                        this.configuration.GetValue<string>("Smtp:Username"), 
                        this.configuration.GetValue<string>("Smtp:Password"));
                    client.Send(message);
                    client.Disconnect(true);
                }
            }
            catch (SmtpProtocolException e)
            {
                throw new SmtpProtocolException("An error occurred during mail sending.", e);
            }
        }

        /// <summary>
        /// Template du mail de confirmation
        /// </summary>
        /// <returns>String</returns>
        private string AccountConfirmationTemplate() =>
            @"
<!doctype html>
<html xmlns=""http://www.w3.org/1999/xhtml"" xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"">
<head>
  <title>
  </title>
  <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
  <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"">
  <meta name=""viewport"" content=""width=device-width, initial-scale=1"">
  <style type=""text/css"">
    #outlook a {
      padding: 0;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }
    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }
    p {
      display: block;
      margin: 13px 0;
    }
  </style>
  <link href=""https://fonts.googleapis.com/css?family=Lato:300,400,500,700"" rel=""stylesheet"" type=""text/css"">
  <style type=""text/css"">
    @import url(https://fonts.googleapis.com/css?family=Lato:300,400,500,700);
  </style>
  <style type=""text/css"">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }
    }
  </style>
  <style media=""screen and (min-width:480px)"">
    .moz-text-html .mj-column-per-100 {
      width: 100% !important;
      max-width: 100%;
    }
  </style>
  <style type=""text/css"">
    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile {
        width: 100% !important;
      }

      td.mj-full-width-mobile {
        width: auto !important;
      }
    }
  </style>
</head>
<body style=""word-spacing:normal;background-color:#e0f2ff;"">
  <div style=""background-color:#e0f2ff;"">
    <div style=""background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"">
      <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""background:#ffffff;background-color:#ffffff;width:100%;"">
        <tbody>
          <tr>
            <td style=""direction:ltr;font-size:0px;padding:0px;padding-top:20px;text-align:center;"">
              <div class=""mj-column-per-100 mj-outlook-group-fix"" style=""font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"">
                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" width=""100%"">
                  <tbody>
                    <tr>
                      <td style=""vertical-align:top;padding:0px;"">
                        <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style width=""100%"">
                          <tbody>
                            <tr>
                              <td align=""center"" style=""font-size:0px;padding:10px 25px;word-break:break-word;"">
                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""border-collapse:collapse;border-spacing:0px;"">
                                  <tbody>
                                    <tr>
                                      <td style=""width:192px;"">
                                        <img alt=""tickets"" height=""auto"" src=""https://res.cloudinary.com/theo-cesi/image/upload/v1643123081/GoodFood/logoGoodFood_zoqjq5.png"" style=""border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"" width=""192"">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td align=""center"" style=""font-size:0px;padding:10px 25px;word-break:break-word;"">
                                <div style=""font-family:Lato, Helvetica, Arial, sans-serif;font-size:25px;line-height:1;text-align:center;color:#8190F5;""><strong>Bienvenue [Firstname] ! <br>
                                    <br> <span style=""color:#061737"">Merci de vous être inscrit sur GoodFood.</span> </strong></div>
                              </td>
                            </tr>
                            <tr>
                              <td align=""center"" style=""font-size:0px;padding:10px 30px;word-break:break-word;"">
                                <div style=""font-family:Lato, Helvetica, Arial, sans-serif;font-size:18px;line-height:1;text-align:center;color:#bdbdbd;""><strong>Pour utiliser nos services dans les meilleurs conditions, merci de valider votre compte en cliquant sur le bouton ci-dessous.</strong></div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=""background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"">
      <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""background:#ffffff;background-color:#ffffff;width:100%;"">
        <tbody>
          <tr>
            <td style=""direction:ltr;font-size:0px;padding:20px 0;text-align:center;"">
              <div class=""mj-column-per-100 mj-outlook-group-fix"" style=""font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"">
                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" width=""100%"">
                  <tbody>
                    <tr>
                      <td style=""vertical-align:top;padding:0px;"">
                        <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style width=""100%"">
                          <tbody>
                            <tr>
                              <td align=""center"" vertical-align=""middle"" style=""font-size:0px;padding:20px 0 0 0;word-break:break-word;"">
                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""border-collapse:separate;line-height:100%;"">
                                  <tr>
                                    <td align=""center"" bgcolor=""#8190F5"" role=""presentation"" style=""border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#8190F5;"" valign=""middle"">
                                      <a href=""[Url]/confirm-account/[MailAddress]/[Token]"" style=""display: inline-block; background: #061737; color: #FFFFFF; font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 120%; margin: 0; text-decoration: none; text-transform: none; padding: 10px 25px; mso-padding-alt: 0px; border-radius: 3px;"" target=""_blank""> Valider mon compte </a>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=""background:#8190F5;background-color:#8190F5;margin:0px auto;max-width:600px;"">
      <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""background:#8190F5;background-color:#8190F5;width:100%;"">
        <tbody>
          <tr>
            <td style=""direction:ltr;font-size:0px;padding:10px;text-align:center;"">
              <div class=""mj-column-per-100 mj-outlook-group-fix"" style=""font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"">
                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" width=""100%"">
                  <tbody>
                    <tr>
                      <td style=""vertical-align:top;padding:0px;"">
                        <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style width=""100%"">
                          <tbody>
                            <tr>
                              <td align=""center"" style=""font-size:0px;padding:10px 25px;word-break:break-word;"">
                                <div style=""font-family:Lato, Helvetica, Arial, sans-serif;font-size:20px;line-height:1;text-align:center;color:#FFFFFF;"">&copy; 2022 - GoodFood</div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>";

        /// <summary>
        /// Template de réinitialisation du mot de passe
        /// </summary>
        /// <returns>String</returns>
        private string GetResetPasswordTemplate() => 
            @"
<!doctype html>
<html xmlns=""http://www.w3.org/1999/xhtml"" xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"">
<head>
  <title>
  </title>
  <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
  <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"">
  <meta name=""viewport"" content=""width=device-width, initial-scale=1"">
  <style type=""text/css"">
    #outlook a {
      padding: 0;
    }
    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }
    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }
    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }
    p {
      display: block;
      margin: 13px 0;
    }
  </style>
  <link href=""https://fonts.googleapis.com/css?family=Lato:300,400,500,700"" rel=""stylesheet"" type=""text/css"">
  <style type=""text/css"">
    @import url(https://fonts.googleapis.com/css?family=Lato:300,400,500,700);
  </style>
  <style type=""text/css"">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }
    }
  </style>
  <style media=""screen and (min-width:480px)"">
    .moz-text-html .mj-column-per-100 {
      width: 100% !important;
      max-width: 100%;
    }
  </style>
  <style type=""text/css"">
    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile {
        width: 100% !important;
      }

      td.mj-full-width-mobile {
        width: auto !important;
      }
    }
  </style>
</head>

<body style=""word-spacing:normal;background-color:#e0f2ff;"">
  <div style=""background-color:#e0f2ff;"">
    <div style=""background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"">
      <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""background:#ffffff;background-color:#ffffff;width:100%;"">
        <tbody>
          <tr>
            <td style=""direction:ltr;font-size:0px;padding:0px;padding-top:20px;text-align:center;"">
              <div class=""mj-column-per-100 mj-outlook-group-fix"" style=""font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"">
                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" width=""100%"">
                  <tbody>
                    <tr>
                      <td style=""vertical-align:top;padding:0px;"">
                        <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style width=""100%"">
                          <tbody>
                            <tr>
                              <td align=""center"" style=""font-size:0px;padding:10px 25px;word-break:break-word;"">
                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""border-collapse:collapse;border-spacing:0px;"">
                                  <tbody>
                                    <tr>
                                      <td style=""width:192px;"">
                                        <img alt=""tickets"" height=""auto"" src=""https://res.cloudinary.com/theo-cesi/image/upload/v1643123081/GoodFood/logoGoodFood_zoqjq5.png"" style=""border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"" width=""192"">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td align=""center"" style=""font-size:0px;padding:10px 25px;word-break:break-word;"">
                                <div style=""font-family:Lato, Helvetica, Arial, sans-serif;font-size:25px;line-height:1;text-align:center;color:#8190F5;""><strong>Bonjour [Firstname] ! <br>
                                    <br> <span style=""color:#061737"">Vous avez fait une demande de réinitialisation de votre mot de passe.</span> </strong></div>
                              </td>
                            </tr>
                            <tr>
                              <td align=""center"" style=""font-size:0px;padding:10px 30px;word-break:break-word;"">
                                <div style=""font-family:Lato, Helvetica, Arial, sans-serif;font-size:18px;line-height:1;text-align:center;color:#bdbdbd;""><strong>Afin de réinitialiser votre mot de passe, merci de cliquer sur le bouton ci-dessous.</strong></div>
                              </td>
                            </tr>
							<tr>
                              <td align=""center"" style=""font-size:0px;padding:10px 30px;word-break:break-word;"">
                                <div style=""font-family:Lato, Helvetica, Arial, sans-serif;font-size:16px;line-height:1;text-align:center;color:#FAB701;""><strong>Attention, ce lien n'est valable que [ValidationTime] heures !</strong></div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=""background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;"">
      <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""background:#ffffff;background-color:#ffffff;width:100%;"">
        <tbody>
          <tr>
            <td style=""direction:ltr;font-size:0px;padding:20px 0;text-align:center;"">
              <div class=""mj-column-per-100 mj-outlook-group-fix"" style=""font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"">
                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" width=""100%"">
                  <tbody>
                    <tr>
                      <td style=""vertical-align:top;padding:0px;"">
                        <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style width=""100%"">
                          <tbody>
                            <tr>
                              <td align=""center"" vertical-align=""middle"" style=""font-size:0px;padding:20px 0 0 0;word-break:break-word;"">
                                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""border-collapse:separate;line-height:100%;"">
                                  <tr>
                                    <td align=""center"" bgcolor=""#8190F5"" role=""presentation"" style=""border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#8190F5;"" valign=""middle"">
                                      <a href=""[Url]/[Token]"" style=""display: inline-block; background: #061737; color: #FFFFFF; font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 120%; margin: 0; text-decoration: none; text-transform: none; padding: 10px 25px; mso-padding-alt: 0px; border-radius: 3px;"" target=""_blank""> Réinitialisation du mot de passe </a>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style=""background:#8190F5;background-color:#8190F5;margin:0px auto;max-width:600px;"">
      <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style=""background:#8190F5;background-color:#8190F5;width:100%;"">
        <tbody>
          <tr>
            <td style=""direction:ltr;font-size:0px;padding:10px;text-align:center;"">
              <div class=""mj-column-per-100 mj-outlook-group-fix"" style=""font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"">
                <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" width=""100%"">
                  <tbody>
                    <tr>
                      <td style=""vertical-align:top;padding:0px;"">
                        <table border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"" style width=""100%"">
                          <tbody>
                            <tr>
                              <td align=""center"" style=""font-size:0px;padding:10px 25px;word-break:break-word;"">
                                <div style=""font-family:Lato, Helvetica, Arial, sans-serif;font-size:20px;line-height:1;text-align:center;color:#FFFFFF;"">&copy; 2022 - GoodFood</div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>";
    }
}