using csharp_goodfood_users.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace csharp_goodfood_users.Services
{
    /// <summary>
    /// Cette classe regroupe les méthodes relatives à la sécurité
    /// </summary>
    public class SecurityService
    {
        /// <summary>
        /// Cette méthode permet d'encoder un mot de passe
        /// </summary>
        /// <param name="password">Mot de passe</param>
        /// <returns>Objet HashSalt</returns>
        public HashSalt EncodePassword(string password)
        {
            byte[] salt = new byte[128/8];

            using (var random = RandomNumberGenerator.Create())
            {
                random.GetBytes(salt);
            }

            string encodedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8 
            ));
            
            return new HashSalt 
            {
                Hash = encodedPassword,
                Salt = salt
            }; 
        }

        /// <summary>
        /// Permet de vérifier qu'un mot de passe est valide
        /// </summary>
        /// <param name="input">Mot de passe entré lors de la connexion</param>
        /// <param name="salt">Salt</param>
        /// <param name="encodedPassword">Mot de passe hashé stocké en base</param>
        /// <returns>bool</returns>
        public bool IsPasswordValid(string input, byte[] salt, string encodedPassword)
        {
            string encoded = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: input,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            ));
            return encoded == encodedPassword;
        }

        /// <summary>
        /// Génération du JWT
        /// Le token est généré par rapport à une clé secrète 
        /// se trouvant dans le fichier "keys/secret.txt"
        /// </summary>
        /// <param name="user">Objet user</param>
        /// <returns>JWT</returns>
        public string GenerateJwtToken(User user)
        {
            try
            {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();    
                Key.SecretKey = "cp7+JNduTOq9RaoYgMqaiqLGFJ22tlsRdbOQ7/b3TCOskkGvHs13wDom8Zqx+1vaZpYK1uAl4U5SV3togoJbUQ==";
                
                byte[] key = Encoding.ASCII.GetBytes(Key.SecretKey);
                
                SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new[]
                    {
                        new Claim("id", user.Id.Trim()),
                        new Claim("mail", user.MailAddress.Trim()),
                        new Claim("roles", string.Join(",", user.Roles).Trim())       
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

                return tokenHandler.WriteToken(token);    
            }
            catch (Exception e)
            {
                throw new ArgumentNullException("An error occurred during JWT generation.", e);
            }
        }
    }
}