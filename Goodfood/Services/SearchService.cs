using csharp_goodfood_users.Models;
using System;
using System.Linq;

namespace csharp_goodfood_users.Services
{
    /// <summary>
    /// SearchService
    /// Encapsulation des méthodes concernant la recherche des utilisateurs
    /// </summary>
    public class SearchService
    {
        private readonly GoodfoodContext context;

        /// <summary>
        /// Constructeur
        /// Initialisation du contexte
        /// </summary>
        /// <param name="context">DbContext</param>
        public SearchService(GoodfoodContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Rechercher un utilisateur par son id
        /// </summary>
        /// <param name="id">Id de l'utilisateur</param>
        /// <returns>Utilisateur</returns>
        public User FindById(string id)
        {
            try
            {
                return context.Users.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception e)
            {
                throw new ArgumentNullException("Erreur lors de la récupération d'un utilisateur par son id.", e);
            }
        }

        /// <summary>
        /// Rechercher un utilisateur par son adresse e-mail
        /// </summary>
        /// <param name="mailAddress">Adresse e-mail de l'utilisateur</param>
        /// <returns>Utilisateur</returns>
        public User FindByMail(string mailAddress)
        {
            try
            {
                return context.Users.FirstOrDefault(x => x.MailAddress == mailAddress);
            }
            catch (Exception e)
            {
                throw new ArgumentNullException("Erreur lors de la récupération d'un utilisateur par son adresse mail.", e);
            }
        }
    }
}